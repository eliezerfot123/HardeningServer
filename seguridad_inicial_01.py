#!/usr/bin/python
# -*- coding: utf-8 -*-

# Seguridad inicial en servidores Debian

#Mejoras pendientes: Agregar rutina para instalacion de iptables. Rutina para envio de correos en caso de que detecte algun cambio en los detectores de rootkit
#Rutina para instalar checklog y configurar.

import os
import pwd

#Funcion para cambiar de puerto al servidor ssh, y evitar conexiones directas con root
def sshd(Npuerto):
    sshPath = '/etc/ssh'
    with open(('%s/sshd_config')%sshPath) as infile:
        with open(('%s/sshd_config_tmp')%sshPath, 'w') as outfile:
            for i,line in enumerate(infile):
                if line.startswith('Port'):
                    outfile.write(('Port %s\n')%Npuerto)
                elif line.startswith('PermitRootLogin'):
                    outfile.write('PermitRootLogin no\n')
                    outfile.write('\nAllowGroups wheel\n')
                    outfile.write('\n###Banner de bienvenida ssh###\n')
                    outfile.write('Banner /etc/issue\n')
                else:
                    outfile.write(line)
    os.system('mv /etc/ssh/sshd_config /etc/ssh/sshd_config.old')
    os.system('mv /etc/ssh/sshd_config_tmp /etc/ssh/sshd_config')
    
 
# Modulo pare agregar direcciones ip permitidas en conexion ssh
def sshIPpermit(ipPermit):
    if ipPermit !='':
        arcSshPermit = open('/etc/hosts.allow', 'a')
        arcSshDeny = open('/etc/hosts.deny', 'a')
        arcSshPermit.write(('sshd: %s\n')%ipPermit)
        arcSshDeny.write('sshd: ALL\n')
        arcSshPermit.close()
        arcSshDeny.close()
        print '\nSe agregaron las direcciones %s para el acceso vía ssh'%ipPermit
        print 'Estos cambios se pueden validar en /etc/hosts.allow\n' 
    else:
        print '------------------¡IMPORTANTE!------------------------\nNo se ha ingresado ninguna dirección IP, por tanto el acceso vía ssh queda abierto a cualquier dirección...\n'

# Modulo para que solo root pueda ejecutar binarios de compilación.        
def rootBin():
    binarios = ['/usr/bin/make','/usr/bin/gcc','/usr/bin/g++','/usr/bin/cc','/usr/bin/as','/usr/bin/lynx','/usr/bin/wget','/usr/bin/curl','/usr/bin/fetch']
    for i in binarios:
        try:
            os.chmod(i,0700)
        except:
            print '%s no encontrado'%i
        else:
            pass 

def bashFalse():
    binFalse = ['bin','sys','sync','games','man','lp','mail','news','uucp','proxy','www-data','backup','list','irc','gnats','nobody','libuuid','daemon']
    for i in binFalse:
        os.system(('chsh -s /bin/false %s')%i)
    print '\nVerificar /etc/passwd los usuarios con acceso a la linea de comandos...'

#Solo root puede ejecutar crontab. Revisar funcionamiento    
def crontab():
    os.system('touch /etc/crontab.allow')
    os.system('touch /etc/at.allow')
   
#Evitar el uso de bombas fork en el sistema, o cualquier sobreprocesamiento...   
def bombaFork():
    arcBombaFork = open('/etc/security/limits.conf','a')
    arcBombaFork.write('* hard nproc  500')
    arcBombaFork.close()

#Limitar el acceso directo a root desde cualquier consola virtual tty
def tty():
    os.system('cp /etc/securetty /etc/securetty.old')
    os.system('rm /etc/securetty')
    os.system('touch /etc/securetty')
    
#Limitar la ejecución de algunos binarios a root
def SUIDroot():
    os.system('chmod -s /bin/ping')
    os.system('chmod -s /usr/bin/chsh')
    os.system('chmod -s /usr/sbin/exim4')
    os.system('chmod -s /bin/umount')
    os.system('chmod -s /bin/mount')
    
#Evitar spoof, ICMP redirect, paquetes marcianos..Configuracion en el kernel del sistem
def sysctl():
    listaSysctl = ['\nnet.ipv4.conf.default.rp_filter=1\n','net.ipv4.conf.all.rp_filter=1\n','net.ipv4.conf.all.accept_redirects = 0\n','net.ipv6.conf.all.accept_redirects = 0\n','net.ipv4.conf.all.send_redirects = 0\n','net.ipv4.conf.all.accept_source_route = 0\n','net.ipv6.conf.all.accept_source_route = 0\n','net.ipv4.conf.all.log_martians = 0\n']
    arcSysctl = open('/etc/sysctl.conf', 'a')
    for i in listaSysctl:
        arcSysctl.write(i)
    arcSysctl.close()

#Camnio de contrase;a cada 45 dias...    
def timePasswd():
    os.system('echo "PASS_MAX_DAYS 45" >> /etc/login.defs')

#Limitar el acceso a root solo para el usuario administrator ingresado por consola.
#La rutina verifica si el usuario existe, de no existir car en un ciclo hasta que se ingrese un usuario valido
#Limitar en tiempo de la sesion de root sin uso (5 min). Configurar fecha y hora del historial para root y usuario administradot
def userAdm(usuario):
    x = False
    while x == False:
        try:
            user = pwd.getpwnam(usuario)
            pathHome =  user.pw_dir
            pathBashrc = os.path.join(pathHome, '.bashrc')
            arcBashrc = open(pathBashrc,'a')
            arcBashrc.write("\nHISTTIMEFORMAT='%F %T '")
            arcBashrc.write("\nHISTCONTROL=erasedups\n")            
            arcBashrc.close()            
            os.system('addgroup --system wheel')
            arcWheel = open('/etc/pam.d/su','a')
            arcWheel.write('\nauth       required   pam_wheel.so group=wheel\n')
            arcWheel.close()            
            os.system(('usermod -a -G wheel %s')%usuario)
            x = True
        except KeyError:
            print '\nUsuario no valido. No se ha encontrado el usuario en el sistema...'
            print 'Por favor ingrese de nuevo el usuario: '
            usuario = raw_input()
            x = False
            
    arcBashrcRoot = open('/root/.bashrc','a')
    arcBashrcRoot.write('\nTMOUT=300\n')
    arcBashrcRoot.write("\nHISTTIMEFORMAT='%F %T '")
    arcBashrcRoot.write('\nHISTCONTROL=erasedups\n')
    arcBashrcRoot.close()
    
def disableIPv6():
    arcIPv6 = open('/etc/default/grub','a')
    arcHosts = open('/etc/hosts','w')
    arcIPv6.write('GRUB_CMDLINE_LINUX="ipv6.disable=1"')
    arcHosts.write('127.0.0.1   localhost')
    arcIPv6.close()
    arcHosts.close()
    os.system('update-grub')
    
#Instalacion y configuracion de detectores de rootkit(tiger, chkrootkit, deborphan) para 
#Auditoria de sistema
def auditorias():
    os.system('aptitude update')
    os.system('aptitude install tiger chkrootkit deborphan htop sudo')
    os.chmod('/var/log/tiger/', 0700)
    os.mkdir('/var/log/auditoria', 0700)
    os.mkdir('/var/log/auditoria/chkrootkit', 0700)
    os.mkdir('/var/log/auditoria/deborphan', 0700)
    os.mkdir('/etc/rutinas', 0700)
    arcRootkit = open('/etc/rutinas/chkrootkit.sh', 'w')
    arcDeborphan = open('/etc/rutinas/deborphan.sh', 'w')
    arcRootkit.write('#!/bin/bash\n#@author: lechuck13\n#email: lechuck13@mail.ru\n## Rutina para ejecutar diariamente el detector de rootkit "chkrootkit"\n\n/usr/sbin/chkrootkit > /var/log/auditoria/chkrootkit/chkrootkit-$(date +%d%m%y)')
    arcDeborphan.write('#!/bin/bash\n#@author: lechuck13\n#email: lechuck13@mail.ru\n## Rutina para ejecutar diariamente el detector de paquetes inutilizados "deborphan"\n\n/usr/bin/deborphan > /var/log/auditoria/deborphan/deborphan-$(date +%d%m%y)')
    arcRootkit.close() ### Pendiente modulo para enviar correos si chkrootkit se ha modificado!!!!
    arcDeborphan.close()
    os.system("echo '5 0 * * * sh /etc/rutinas/chkrootkit.sh\n10 0 * * * sh /etc/rutinas/deborphan.sh' | crontab -")
    
       
         
os.system('clear')
if os.getuid() ==0:
    print '\nEjecutando cambios en servicio ssh'
    print '\nPor favor ingrese el puerto ssh a asignar en el servidor'
    sshd(raw_input())
    print '\nEjecutando cambio de puerto servidor ssh...'
    print '\nIngrese las direcciones IP permitidas para accesar via ssh:'
    print 'Si son más de una dirección, ingrese la dirección separado de un espacio simple'
    print "Si no se desea controlar las direcciones ip que ingresan vía ssh, presione enter...\n"
    sshIPpermit(raw_input())
    rootBin()
    print 'Eliminando acceso a linea de comando shell en usuarios de sistema...\n'
    bashFalse()
    print 'Limitando a root la ejecución de tareas programadas con crontab...'
    crontab()
    print 'Limitando uso de bomba fork...'
    bombaFork()
    print 'Limitando el acceso directo a root desde cualquier consola virtual...'
    tty()
    print 'Limitando el SUID a root de algunos binarios...'
    SUIDroot()
    print 'Hablilitabdo el kernel para protección contra spoof, ICMP redirect, paquetes marcianos, entre otros...'
    sysctl()
    print 'Habilitando el cambio periodico de claves...'
    timePasswd()
    print 'Limitando el acceso de usuarios a root...'
    print '\nIngrese el nombre del usuario que tendrá acceso a root: '
    userAdm(raw_input())
    print '\nLimitando uso de IPv6...'
    disableIPv6()
    print 'Iniciando la instalación de paquetes de auditoria...'
    auditorias()
    print '\nFinalizando las configuraciones iniciales de seguridad... '
    print '\n\nPara que los cambios surtan efecto es necesario reiniciar la máquina...'
    print '\n¿Desea reiniciar la máquina ahora (SI/NO)? '
    resp = raw_input()
    while resp !='SI' and resp !='NO':
        print 'Entrada no válida. Ingrese "SI" o "NO"'
        resp = raw_input()
    if resp == 'SI':
        print 'REINICIANDO...'
        os.system('reboot')
    else:
        print 'Para que los cambios tomen efecto debe de reiniciar el sistema...\n'
    
        
else: 
    print '\nError: Necesitas ser root para correr el script\n'
   
    

