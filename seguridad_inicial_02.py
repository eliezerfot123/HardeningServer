#!/usr/bin/python
# -*- coding: utf-8 -*-


#Continuación del script seguridad_inicial_01.py 
#Ejecución luego de ejecutar seguridad_inicial_01.py y reiniciar la maquina

import os

def auditoriaBase():
    os.system('/usr/sbin/chkrootkit > /var/log/auditoria/chkrootkit/chkrootkit-base')
    os.system('/usr/bin/deborphan > /var/log/auditoria/deborphan/deborphan-base')
    os.system('tiger')
    
def eraseHistory():
    arcHistory = open('/root/.bash_history','w')
    arcHistory.write('')
    arcHistory.close()

os.system('clear')
if os.getuid() ==0:
    print 'Continuación del script seguridad_inicial_01.py...'
    print 'Ejecutando la auditoria base del sistema...'
    auditoriaBase()
    print '\nEliminando historial de cambios...'
    eraseHistory()
    print '\n\nPara que los cambios surtan efecto es necesario reiniciar la máquina...'
    print '\n¿Desea reiniciar la máquina ahora (SI/NO)? '
    resp = raw_input()
    while resp !='SI' and resp !='NO':
        print 'Entrada no válida. Ingrese "SI" o "NO"'
        resp = raw_input()
    if resp == 'SI':
        print 'REINICIANDO...'
        os.system('reboot')
    else:
        print 'Para que los cambios tomen efecto debe de reiniciar el sistema...\n'
else: 
    print '\nError: Necesitas ser root para correr el script\n'
   
    
